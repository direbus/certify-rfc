## Digital [type here] Card

Principals:

- Verifiable, offline* and online.
- Minimal Personally Identifiable Information (PII) on Card
- Hard-to-guess IDs

## Design

- Card Type:
  - [ISO/IEC 7810](https://en.wikipedia.org/wiki/ISO/IEC_7810) (no-chip)
  - [ISO/IEC 7816](https://en.wikipedia.org/wiki/ISO/IEC_7816) contactless smartcard
    - store token (publicly, as public data)
    - store several PII data on encryption
    - public/secret crypto for “challenge” capability → (might be) usefull for [Biometric Passport](https://en.wikipedia.org/wiki/Biometric_passport) Verification
- Front Face
  - Contains:
    - Name, photo, sex\*, and species* of partners
      - `sex` field should be based on bearer’s biology. Possible values are: `male`, `female`, `other` or none (`null`).
      - `species` field can be changed to `race`, depending on country’s jurisdiction whether such information should be disclosed publicly or legally available.
        Possible values for `species`: `human`, `other`, `virtual`, `virtual+human`, etc...
    - Issued Date
    - Serial (from `jti` for quick identification) → should use monospace font!
    - Verification Code (treated same as CVV) → should use monospace font!
    - Relevant, important symbols and logo (such as biometric symbol)
    - RootCA’s relevant disclaimer (“This card are issued in compliance of ....”)
    - All information that printed on this face should be treated as “public-only” data, except the Verification Code.
- Back Face:
  - Contains:
    - Token as a QR
      - QR allowed to be branded
    - Issuer name and ID → should use monospace font!
    - Issue date → should use monospace font!
    - Expiry date → should use monospace font!
    - Certify’s logo
    - Other relevant logo (NFC, etc)
    - Issuers’ relevant disclaimer (“Card issued by ..., if you found this card please return to...”)
    - RootCA’s verification disclaimer (“Verify card’s authenticity on certify.my.id/please”)
- Token:
  - Based on [RFC7515](https://datatracker.ietf.org/doc/html/rfc7515)
  - Claim must comply [RFC7519 Section 4.1](https://datatracker.ietf.org/doc/html/rfc7519#section-4.1) or extends them. For full claim registry, please see https://www.iana.org/assignments/jwt/jwt.xhtml.
  - Token Schema for `type=marriage`
    - `subs` (2 or more, depends on what the media shows; array of)
      - `name`  (limited full name, string)
      - `uid` (string)
      - `sex` (char, `m`, `f`, or `o`. Genderless can use `null` instead.)
      - `spcs` (or `race`, int/long, [optional])
      - Additional public info like “District, City, Province” info for offline, manual authentication, [optional].
    - `jti`, JWT ID (also called as card’s serial)
    - `pid`, family ID
    - `iss`, issuer (just the id)
    - `iat`, issued at
    - `nbf`, not before
    - `exp`, expiry (if exists)
    - `kid`, key id (placed on token header)
    - `type` = `marriage` (possible values: `individual`, `marriage`, other?)
    - `ver` = `1`
- Smartcard Content
  - Issuer’s full-chain certificate.
  - Signed token (from above)
  - PKI Secret+Public key(?)



## General Do and Don’ts 

- Do NOT
  - Use magnetic stripe for cards.
- DO
  - Capture QR for verification, BUT NOT the Verification Code.

## To learn:

- https://www.w3.org/TR/2019/REC-vc-data-model-20191119/